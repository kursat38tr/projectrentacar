###FROM openjdk:15
###ADD target/demo-0.0.1-SNAPSHOT.jar demo-0.0.1-SNAPSHOT.jar
###RUN bash -c 'touch /demo-0.0.1-SNAPSHOT.jar'
###EXPOSE 8080
###ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom","-Dspring.profiles.active=container", "-jar", "demo-0.0.1-SNAPSHOT.jar"]
##
###FROM openjdk:15
###VOLUME /tmp
###ADD /target/demo.jar app.jar
###ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-Dspring.profiles.active=container", "-jar","/app.jar"]
###FROM openjdk:15
###VOLUME /tmp
###COPY target/demo-0.0.1-SNAPSHOT.jar demo-0.0.1-SNAPSHOT.jar
###CMD java -jar demo-0.0.1-SNAPSHOT.jar
###EXPOSE 8080
###ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-Dspring.profiles.active=container", "-jar","/app.jar"]
##
###FROM openjdk:15
###EXPOSE 8080
###ADD target/demo-0.0.1-SNAPSHOT.jar.jar demo-0.0.1-SNAPSHOT.jar.jar
###ENTRYPOINT ["java" , " -jar, ", "demo-0.0.1-SNAPSHOT.jar.jar"]
##
##FROM maven:3.6.3-jdk-15 as builder
##COPY  . /root/app/
##WORKDIR /root/app
##RUN mvn install
##
##
##FROM openjdk:15 as jdk15
##EXPOSE 8080
##COPY --from=builder /root/app/ /home/app/
##WORKDIR /home/app
##ENTRYPOINT ["java","-jar", "-Xmx15m", "./target/demo-0.0.1-SNAPSHOT.jar"]
#
##FROM openjdk:15
##VOLUME /tmp
##ADD /target/demo-0.0.1-SNAPSHOT.jar app.jar
##ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","f/app.jar"]
##FROM openjdk:15 AS builder
##WORKDIR target/dependency
##ARG APPJAR=target/*.jar
##COPY ${APPJAR} app.jar
##RUN jar -xf ./app.jar
##
##FROM openjdk:15
##VOLUME /tmp
##ARG DEPENDENCY=target/dependency
##COPY --from=builder ${DEPENDENCY}/BOOT-INF/lib /app/lib
##COPY --from=abuilder ${DEPENDENCY}/META-INF /app/META-INF
##COPY --from=builder ${DEPENDENCY}/BOOT-INF/classes /app
##ENTRYPOINT ["java","-cp","app:aapp/lib/*","com.projectrentacar.demo.DemoApplication"]
#
##FROM openjdk:15
##VOLUME /tmp
##ADD demo-0.0.1-SNAPSHOT.jar app.jar
##ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
#
#FROM openjdk:11
#VOLUME /tmp
#WORKDIR target/dependency
#ARG APPJAR=target/*.jar
#ADD ${APPJAR} app.jar
#ENTRYPOINT ["java","-Djavah.security.egd=file:/dev/./urandom","-jar","/app.jar"]

FROM openjdk:11
VOLUME /tmp
ADD /target/demo-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
