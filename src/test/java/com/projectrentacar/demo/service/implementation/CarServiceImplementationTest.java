package com.projectrentacar.demo.service.implementation;

import com.projectrentacar.demo.domain.Car;
import com.projectrentacar.demo.domain.CarCategory;
import com.projectrentacar.demo.repository.CarCategoryRepository;
import com.projectrentacar.demo.repository.CarRepository;
import org.checkerframework.checker.units.qual.C;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;


class CarServiceImplementationTest {


    private final CarCategoryRepository carCategoryRepository = Mockito.mock(CarCategoryRepository.class);


    private final CarRepository carRepository = Mockito.mock(CarRepository.class);


    private CarServiceImplementation carServiceImplementation;

    @Test
    void getCars() {

        ArrayList<Car> cars = new ArrayList<Car>();
        var carCategory = new CarCategory(1L, "Audi");

        cars.add(new Car(2L, carCategory, "test1", "test11", "test123", "gas", 145.00, true));
        cars.add(new Car(2L, carCategory, "test2", "test12", "test234", "gas", 145.00, true));
        cars.add(new Car(2L, carCategory, "test3", "test13", "test567", "gas", 145.00, true));

        CarServiceImplementation carServiceImplementation = new CarServiceImplementation(carRepository, carCategoryRepository);
        Mockito.when(carRepository.findAll()).thenReturn(cars);

        List<Car> expectedCars = carServiceImplementation.getCars();

        Assertions.assertEquals(expectedCars, cars);
    }

    @Test
    void postCar() {

        CarServiceImplementation carServiceImplementation = new CarServiceImplementation(carRepository, carCategoryRepository);
        var newCarCategory = new CarCategory(1L, "Audi");
        Car newCar = new Car(2L, newCarCategory, "test1", "test11", "test123", "gas", 111.00, true);

        Long id = null;
        String licensePlate = "test1";
        String name = "test11";
        String description = "test123";
        String fuel = "gas";
        Double currentPrice = 111.00;


        Mockito.when(carServiceImplementation.addNewCar(licensePlate, name, description, fuel, currentPrice, true,6L)).thenReturn(newCar);



    }

    @Test
    void addNewCar() {
    }

    @Test
    void update() {
    }

    @Test
    void delete() {
    }
}
