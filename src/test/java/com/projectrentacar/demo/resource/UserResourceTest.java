package com.projectrentacar.demo.resource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.projectrentacar.demo.domain.User;
import com.projectrentacar.demo.domain.UserPrincipal;
import com.projectrentacar.demo.enumeration.Role;
import com.projectrentacar.demo.filter.JwtAccessDeniedHandler;
import com.projectrentacar.demo.filter.JwtAuthenticationEntryPoint;
import com.projectrentacar.demo.filter.JwtAuthorizationFilter;
import com.projectrentacar.demo.repository.CarCategoryRepository;
import com.projectrentacar.demo.repository.CarRepository;
import com.projectrentacar.demo.service.implementation.CarCategoryImplementation;
import com.projectrentacar.demo.service.implementation.CarServiceImplementation;
import com.projectrentacar.demo.service.implementation.UserServiceImplementation;
import com.projectrentacar.demo.utility.JWTTokenProvider;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import org.springframework.http.MediaType;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.test.context.support.WithMockUser;


import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import org.springframework.web.context.WebApplicationContext;


import java.util.Arrays;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")

public class UserResourceTest {

    @MockBean
    private UserServiceImplementation userServiceImplementation;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private AuthenticationManager authenticationManager;


    @BeforeEach
    public void setup(){
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).apply(springSecurity()).build();
    }
////    @BeforeEach
////    public void setup() {
////        mockMvc = MockMvcBuilders
////                .webAppContextSetup(webApplicationContext)
////                .apply(springSecurity())
////                .alwaysDo(print())
////                .build();
////    }
//
////    @BeforeEach
////    public void setup() {
////        mockMvc = MockMvcBuilders
////                .webAppContextSetup(webApplicationContext)
////
////                .apply(springSecurity()) // enable security for the mock set up
////                .build();
////    }
//
////    public UserResourceTest(){}
//    // CI/CD PROJECT FAIL
////    @Test
////    public void NotUnauthorized() throws Exception {
////        mockMvc.perform(MockMvcRequestBuilders.get("/user")
////                .param("email", EMAIL))
////                .andExpect(status().isUnauthorized());
////    }
//    @Test
//    public void shouldReturnAJWTToken() throws Exception{
//
//        String jsonFormat = "{'username':'kursat38tr','password':'1K4gUw0mlG'}".replace("'", "\"");
//
//        mockMvc.perform(MockMvcRequestBuilders.post("/user/login")
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(jsonFormat))
//                .andExpect(status().isOk());
//    }
//
//
//
//    @Test
//    public void givenToken_whenPostGetSecureRequest_thenOk() throws Exception {
//
//        var user = new User("admin", "oZys5uxWxr");
//        UserPrincipal userPrincipal = new UserPrincipal(user);
//        JWTTokenProvider jwtTokenProvider = new JWTTokenProvider();
//        String accessToken = jwtTokenProvider.generateJwtToken(userPrincipal);
//
//
//
//        mockMvc.perform(post("/user/login")
//                .header("Authorization", "Bearer " + accessToken)
//                .contentType(MediaType.APPLICATION_JSON));
//
//
////        mockMvc.perform(get("/employee")
////                .param("email", "jim@yahoo.com")
////                .header("Authorization", "Bearer " + accessToken)
////                .accept("application/json;charset=UTF-8"))
////                .andExpect(status().isOk())
////                .andExpect(content().contentType(application/json;charset=UTF-8))
////      .andExpect(jsonPath("$.name", is("Jim")));
//    }
//
//    @Test
//    void login() throws Exception {
//        var user = new User("admin", "oZys5uxWxr");
//        UserPrincipal userPrincipal = new UserPrincipal(user);
//
//
////        var user = new User("kursat38tr", "1K4gUw0mlG");
//
////        mockMvc.perform(MockMvcRequestBuilders.post("/user/login"))
////                .andExpect(MockMvcResultMatchers.status().isOk())
////                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
////                .andExpect(MockMvcResultMatchers.jsonPath("$.size()", Matchers.is(2)))
////                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id", Matchers.is(1)))
////                .andExpect(MockMvcResultMatchers.jsonPath("$[0].category_name", Matchers.is("Audi")))
////                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id", Matchers.is(2)))
////                .andExpect(MockMvcResultMatchers.jsonPath("$[1].category_name", Matchers.is("Mercedes")));
//
//        String jsonFormat = "{'username':'kursat38tr','password':'1K4gUw0mlG'}".replace("'", "\"");
//        String jsonRequest= objectMapper.writeValueAsString(user);
//
//        MvcResult result =  mockMvc.perform(post("/user/login").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().is(200)).andReturn();
//
//        String jwt = result.getResponse().getContentAsString();
//
//        Assertions.assertEquals(200, result.getResponse().getStatus());
//
//    }
//
//    @Test
//    void register() throws Exception {
//
//        var user = new User("Kursat12", "Dogan12", "kursat38tr10",  "kursat38tr10@hotmail.com" );
//
//        String jsonRequest= objectMapper.writeValueAsString(user);
//
//        MvcResult result =  mockMvc.perform(post("/user/register").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().is(200)).andReturn();
//
//        assertEquals(200, result.getResponse().getStatus());
//
//    }
//
//    @Test
//    void addNewUser() {
//
//
//    }
//
//    @Test
//    void update() {
//
//    }
//
//    @Test
//    void getUser() {
//    }

    @Test
    @WithMockUser("/admin-1")
    void getAllUsers() throws Exception {

        var authorities = new String[]{"user:read", "user:update", "user:create", "user:delete"};
        var user1 = new User(2L, "12345", "Kursattest", "Dogan12", "kursat38tr1"
                , "test123", "kursat38tr1@hotmail.com", "kursat" , new Date(), new Date()
                , new Date(), Role.ROLE_USER.name(), authorities, true, true );
        var user2 = new User(3L, "12346", "Kursattest2", "Dogan12", "kursat38tr2"
                , "test123", "kursat38tr2@hotmail.com", "kursat" , new Date(), new Date()
                , new Date(), Role.ROLE_USER.name(), authorities, true, true );

        Mockito.when(userServiceImplementation.getUsers()).thenReturn(Arrays.asList(user1, user2));


        mockMvc.perform(MockMvcRequestBuilders.get("/user/list"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON ))
                .andExpect(MockMvcResultMatchers.jsonPath("$.size()", Matchers.is(2)));
    }
}
