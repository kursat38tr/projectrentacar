package com.projectrentacar.demo.resource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.TypeResolutionContext;
import com.projectrentacar.demo.domain.Car;
import com.projectrentacar.demo.domain.CarCategory;
import com.projectrentacar.demo.service.implementation.CarServiceImplementation;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class CarResourceTests {

//    @MockBean
//    private CarResource carResource;
//
//    @MockBean
//    private CarServiceImplementation carServiceImplementation;


    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;


    @Autowired
    private WebApplicationContext webApplicationContext;

    @BeforeEach
    public void setup(){
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).apply(springSecurity()).build();
    }


    @Test
    void getAllCars() throws Exception {

        ArrayList<Car> cars = new ArrayList<Car>();

        MvcResult result = mockMvc.perform(get("/cars/list")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        List<Car> car = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<Car>>() {});
        Assertions.assertEquals(cars, car);
    }

//    @Test
//    void addNewCar() throws Exception {
//
//        var carCategory = new CarCategory(6L, "Audi");
//        Car newCar = Car.builder()
//                .id(20L)
//                .category(new CarCategory(6L))
//                .licensePlate("test1")
//                .name("Test11")
//                .description("test123")
//                .fuel("gas")
//                .currentPrice(145.00)
//                .availability(true)
//                .build();
//
//
//        String jsonRequest1= objectMapper.writeValueAsString(carCategory);
//        String jsonRequest= objectMapper.writeValueAsString(newCar);
//
//
//        MvcResult result = mockMvc.perform(post("/cars/createCar")
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(objectMapper.writeValueAsString(newCar)))
//                .andExpect(status().isOk())
//                .andReturn();
//
//      Car car = objectMapper.readValue(result.getResponse().getContentAsString(), Car.class);
//      Assertions.assertEquals(newCar, car);
//    }
}
