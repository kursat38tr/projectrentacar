package com.projectrentacar.demo.api.tests.repository;

import com.projectrentacar.demo.domain.User;
import com.projectrentacar.demo.enumeration.Role;
import com.projectrentacar.demo.repository.UserRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;


import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;



@DataJpaTest
@ActiveProfiles("test")

public class UserRepositoryTest {

    @Autowired
    private  UserRepository userRepository;

//    @Test
//    public void saveUser(){
//        var authorities = new String[]{"user:read", "user:update", "user:create", "user:delete"};
//        var user = new User(null, "12345", "Kursat12", "Dogan12", "kursat38tr", "test123", "kursat38tr@hotmail.com", "kursat" , new Date(), new Date() , new Date(), Role.ROLE_USER.name(), authorities, true, true );
//
//        User saveUser = userRepository.save(user);
//        assertThat(saveUser).usingRecursiveComparison().ignoringFields("id").isEqualTo(user);
//    }


    @Test
    void findUserByUsername() {

    }

    @Test
    void findUserByEmail() {

    }
}
