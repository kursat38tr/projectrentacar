package com.projectrentacar.demo.api.tests.resource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.projectrentacar.demo.domain.CarCategory;
import com.projectrentacar.demo.service.implementation.CarCategoryImplementation;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.context.WebApplicationContext;

import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class CarCategoryResourceTest {

    @MockBean
    private CarCategoryImplementation carCategoryImplementation;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Test
    @WithMockUser("/admin-1")
    void getAllCategories() throws Exception {

    var category1 = new CarCategory(1L, "Audi");
    var category2 = new CarCategory(2L, "Mercedes");


        Mockito.when(carCategoryImplementation.getCategories()).thenReturn(Arrays.asList(category1, category2));

        mockMvc.perform(MockMvcRequestBuilders.get("/carCategory/list"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.size()", Matchers.is(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].categoryName", Matchers.is("Audi")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id", Matchers.is(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].categoryName", Matchers.is("Mercedes")));

    }

    @Test
    @WithMockUser("/admin-1")
    void addNewCarCategory() throws Exception {

        given(carCategoryImplementation.create(any(CarCategory.class))).willAnswer((invocation -> invocation.getArgument(0)));

        CarCategory carCategory = new CarCategory(1L, "BMW");

        this.mockMvc.perform(post("/carCategory/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(carCategory)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", Matchers.is(1)))
                .andExpect(jsonPath("$.categoryName", Matchers.is(carCategory.getCategoryName())));
    }
}
