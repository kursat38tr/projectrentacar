package com.projectrentacar.demo.api.tests.resource;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.projectrentacar.demo.domain.User;
import com.projectrentacar.demo.enumeration.Role;
import com.projectrentacar.demo.service.implementation.UserServiceImplementation;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class UserTest {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UserServiceImplementation userServiceImplementation;

    @Autowired
    private MockMvc mvc;



    @Test
    @DisplayName("Failed to Authenticate, Wrong username and password ")
    public void shouldReturnAJWTToken() throws Exception{

        var user = new User("admin", "oZys5uxWxr");

        mvc.perform(MockMvcRequestBuilders.post("/user/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(user)))
                .andExpect(status().isBadRequest());
    }

    @Test
    @DisplayName("Failed to Authenticate, Wrong username and password ")
    public void login() throws Exception{

        var user = new User("kursat38tr10", "weTkQN1kv4");

        var result = mvc.perform(MockMvcRequestBuilders.post("/user/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(user)))
                .andExpect(status().isOk())
                .andReturn();

        String jwtToken = result.getResponse().getContentAsString();
    }

//    @Test
//    @DisplayName("User cannot access Endpoint, Not logged in ")
//    public void Cannot_Access_End_Point() throws Exception{
//
//        var result = mvc.perform(MockMvcRequestBuilders.post("/user/add")
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isForbidden())
//                .andReturn();
//
//        String jwtToken = result.getResponse().getContentAsString();
//    }

//    @Test
//    @DisplayName("Failed to Authenticate, Wrong username and password ")
//    public void shouldReturnAJWTToke1n() throws Exception{
//
//        var user = new User("kursat38tr", "1K4gUw0mlG");
//
//        var result = mvc.perform(MockMvcRequestBuilders.post("/user/login")
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(objectMapper.writeValueAsString(user)))
//                .andExpect(status().isOk())
//                .andDo(print())
//                .andReturn();
//
//        String jwtToken = result.getResponse().getContentAsString();
//
//
//
////        mvc.perform(MockMvcRequestBuilders.get("/user/add")
////                .contentType(MediaType.APPLICATION_JSON)
////                .header("Jwt-Token", "Bearer " + jwtToken))
////                .andExpect(status().isOk()); //THIS SHOULD PASS
//
//    }

    @org.junit.jupiter.api.Test
    @DisplayName("Username already Exist ")
    public void register() throws Exception {

        var user = new User("Kursat12", "Dogan12", "kursat38tr10",  "kursat38tr10@hotmail.com" );

        String jsonRequest= objectMapper.writeValueAsString(user);

        MvcResult result =  mvc.perform(post("/user/register").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(400)).andReturn();

        assertEquals(400, result.getResponse().getStatus());

    }

//    @Test
////    @org.junit.jupiter.api.Test
//    @WithMockUser("/admin-1")
//    public void getAllUsers() throws Exception {
//
//        var authorities = new String[]{"user:read", "user:update", "user:create", "user:delete"};
//        var user1 = new User(2L, "12345", "Kursattest", "Dogan12", "kursat38tr1"
//                , "test123", "kursat38tr1@hotmail.com", "kursat" , new Date(), new Date()
//                , new Date(), Role.ROLE_USER.name(), authorities, true, true );
//        var user2 = new User(3L, "12346", "Kursattest2", "Dogan12", "kursat38tr2"
//                , "test123", "kursat38tr2@hotmail.com", "kursat" , new Date(), new Date()
//                , new Date(), Role.ROLE_USER.name(), authorities, true, true );
//
//        Mockito.when(userServiceImplementation.getUsers()).thenReturn(Arrays.asList(user1, user2));
//
//
//        mvc.perform(MockMvcRequestBuilders.get("/user/list"))
//                .andExpect(MockMvcResultMatchers.status().is(200))
//                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON ))
//                .andExpect(MockMvcResultMatchers.jsonPath("$.size()", Matchers.is(2)));
//    }

}
