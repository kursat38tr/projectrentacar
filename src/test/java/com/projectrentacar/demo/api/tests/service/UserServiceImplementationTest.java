package com.projectrentacar.demo.api.tests.service;

import com.projectrentacar.demo.domain.User;
import com.projectrentacar.demo.enumeration.Role;
import com.projectrentacar.demo.repository.UserRepository;
import com.projectrentacar.demo.service.EmailService;
import com.projectrentacar.demo.service.LoginAttemptService;
import com.projectrentacar.demo.service.implementation.UserServiceImplementation;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Date;

class UserServiceImplementationTest {

    private final UserRepository userRepository = Mockito.mock(UserRepository.class);
    private final BCryptPasswordEncoder passwordEncoder = Mockito.mock(BCryptPasswordEncoder.class);
    private final LoginAttemptService loginAttemptService = Mockito.mock(LoginAttemptService.class);
    private final EmailService emailService = Mockito.mock(EmailService.class);


    @Test
    @DisplayName("Should Load By Username")
    void loadUserByUsername() {

        UserServiceImplementation userServiceImplementation = new UserServiceImplementation(userRepository, passwordEncoder, loginAttemptService, emailService);

        var authorities = new String[]{"user:read", "user:update", "user:create", "user:delete"};

        var user = new User(1L, "12345", "Kursat12", "Dogan12", "kursat38tr", "test123", "kursat38tr@hotmail.com", "kursat" , new Date(), new Date() , new Date(), Role.ROLE_USER.name(), authorities, true, true );

    }

    @Test
    void register() {
    }

    @Test
    void addNewUser() {
        UserServiceImplementation userServiceImplementation = new UserServiceImplementation(userRepository, passwordEncoder, loginAttemptService, emailService);

        var authorities = new String[]{"user:read", "user:update", "user:create", "user:delete"};

        var user = new User(1L, "12345", "Kursat12", "Dogan12", "kursat38tr", "test123", "kursat38tr@hotmail.com", "kursat" , new Date(), new Date() , new Date(), Role.ROLE_USER.name(), authorities, true, true );

    }

    @Test
    void updateUser() {
    }

    @Test
    void resetPassword() {
    }

    @Test
    void updateProfileImage() {
    }

    @Test
    void getUsers() {
    }

    @Test
    void findUserByUsername() {
    }

    @Test
    void findUserByEmail() {
    }

    @Test
    void deleteUser() {
    }
}