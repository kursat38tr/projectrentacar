DROP TABLE IF EXISTS user;

CREATE TABLE user (
                         id INT AUTO_INCREMENT  PRIMARY KEY,
                         authorities tinyblob DEFAULT NULL,
                         first_name varchar(255) DEFAULT NULL,
                         is_active bit(1) DEFAULT NULL,
                         is_not_locked bit(1) DEFAULT NULL,
                         email varchar(255) DEFAULT NULL,
                         join_date datetime(6) DEFAULT NULL,
                         last_login_date datetime(6) DEFAULT NULL,
                         last_login_date_display datetime(6) DEFAULT NULL,
                         last_name varchar(255) DEFAULT NULL,
                         password varchar(255) DEFAULT NULL,
                         profile_image_url varchar(255) DEFAULT NULL,
                         role varchar(255) DEFAULT NULL,
                         user_id varchar(255) DEFAULT NULL,
                         username varchar(255) DEFAULT NULL
);

INSERT INTO user (username, authorities, password, is_active, is_not_locked) VALUES
('kursat', DEFAULT, '$2a$10$eJ6vDXmV/xV2N0on7xh/AOL0waqgd6PqMMuF0tdDxpTZ7HoCCmDWK', true, true);
