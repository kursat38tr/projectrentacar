package com.projectrentacar.demo.service;

import com.projectrentacar.demo.domain.Car;

public interface CarService {

    Car postCar(Car car);

        // Car addNewCar(Boolean availability, Double currentPrice, String description, String fuel, String imageUrl, String licensePlate, String name, Long categoryId );
    Car addNewCar(String licensePlate, String name, String description, String fuel, Double currentPrice, Boolean availability, Long category);
}
