package com.projectrentacar.demo.service.implementation;


import com.projectrentacar.demo.domain.Car;
import com.projectrentacar.demo.domain.CarCategory;
import com.projectrentacar.demo.repository.CarCategoryRepository;
import com.projectrentacar.demo.service.CarCategoryService;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@Configuration
public class CarCategoryImplementation implements CarCategoryService {

    private final CarCategoryRepository carCategoryRepository;

    public CarCategoryImplementation(CarCategoryRepository carCategoryRepository) {
        this.carCategoryRepository = carCategoryRepository;
    }

    @Override
    public List<CarCategory> getCategories() {
        return carCategoryRepository.findAll();
    }


    public CarCategory create(CarCategory car) {
        CarCategory newCarCategory = new CarCategory();
        newCarCategory.setCategoryName(car.getCategoryName());
        carCategoryRepository.save(newCarCategory);
        return newCarCategory;
    }


    public CarCategory update(Long id, CarCategory carCategoryDetails){
        CarCategory carCategory = carCategoryRepository.findCategoryById(id);
        carCategory.setCategoryName(carCategoryDetails.getCategoryName());
        carCategoryRepository.save(carCategory);
        return carCategory;
    }

    public void delete(Long id){
        CarCategory carCategory = carCategoryRepository.findCategoryById(id);
        carCategoryRepository.delete(carCategory);
    }
}
