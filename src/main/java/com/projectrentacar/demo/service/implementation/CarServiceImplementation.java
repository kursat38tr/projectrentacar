package com.projectrentacar.demo.service.implementation;

import com.projectrentacar.demo.domain.Car;
import com.projectrentacar.demo.domain.CarCategory;
import com.projectrentacar.demo.repository.CarCategoryRepository;
import com.projectrentacar.demo.repository.CarRepository;
import com.projectrentacar.demo.service.CarService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;


@Service
@Transactional
@Configuration
public class CarServiceImplementation implements CarService {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());
    private final CarRepository carRepository;
    private final CarCategoryRepository carCategoryRepository;

    @Autowired
    public CarServiceImplementation(CarRepository carRepository, CarCategoryRepository carCategoryRepository) {
        this.carRepository = carRepository;
        this.carCategoryRepository = carCategoryRepository;
    }

    public List<Car> getCars() {
        return carRepository.findAll();
    }

    @Override
    public Car postCar(Car car) {
        Car newCar = new Car();
        newCar.setLicensePlate(car.getLicensePlate());
        newCar.setName(car.getName());
        newCar.setDescription(car.getDescription());
        newCar.setFuel(car.getFuel());
        newCar.setAvailability(car.getAvailability());
        newCar.setCurrentPrice(car.getCurrentPrice());
        newCar.setCategory(car.getCategory());
        carRepository.save(newCar);
        return newCar;
    }

    @Override
    public Car addNewCar(String licensePlate, String name, String description, String fuel, Double currentPrice, Boolean availability, Long category) {
        var car= new Car();
        var categoryId = carCategoryRepository.findCategoryById(category);
        car.setCategory(categoryId);
        car.setLicensePlate(licensePlate);
        car.setName(name);
        car.setDescription(description);
        car.setFuel(fuel);
        car.setCurrentPrice(currentPrice);
        car.setAvailability(availability);
        carRepository.save(car);
        return car;
    }

    public Car update(Long id, Car carDetails){
        Car car = carRepository.findCarById(id);
        car.setLicensePlate(carDetails.getLicensePlate());
        car.setName(carDetails.getName());
        car.setDescription(carDetails.getDescription());
        car.setFuel(carDetails.getDescription());
        car.setCurrentPrice(carDetails.getCurrentPrice());
        car.setAvailability(carDetails.getAvailability());
        car.setCategory(carDetails.getCategory());
        carRepository.save(car);
        return car;
    }

    public void delete(Long id){
        Car car = carRepository.findCarById(id);
        carRepository.delete(car);
    }
}
