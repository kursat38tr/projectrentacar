package com.projectrentacar.demo.service;

import com.projectrentacar.demo.domain.CarCategory;

import java.util.List;

public interface CarCategoryService {
    List<CarCategory> getCategories();

}
