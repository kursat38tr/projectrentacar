package com.projectrentacar.demo.resource;


import com.projectrentacar.demo.domain.CarCategory;
import com.projectrentacar.demo.domain.HttpResponse;
import com.projectrentacar.demo.service.CarCategoryService;
import com.projectrentacar.demo.service.implementation.CarCategoryImplementation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/carCategory")
public class CarCategoryResource {

    private final CarCategoryImplementation carCategoryImplementation;
    private final CarCategoryService carCategoryService;

    public CarCategoryResource(CarCategoryImplementation carCategoryImplementation, CarCategoryService carCategoryService) {
        this.carCategoryImplementation = carCategoryImplementation;
        this.carCategoryService = carCategoryService;
    }

    @GetMapping(path = "/list")
    public ResponseEntity<List<CarCategory>> getAllCategories(){
        List<CarCategory> categories = carCategoryService.getCategories();
        return new ResponseEntity<>(categories, OK);
    }

    @PostMapping(path = "/create")
    public ResponseEntity<CarCategory> addNewCar(@RequestBody CarCategory carCategory)
    {
        CarCategory newCarCategory = carCategoryImplementation.create(carCategory);
        return new ResponseEntity<>(newCarCategory, CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody CarCategory carCategory, @PathVariable Long id) {
        try {
            var updatedCar = carCategoryImplementation.update(id, carCategory);
            return new ResponseEntity<>(updatedCar, OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<HttpResponse> delete(@PathVariable(value = "id") Long id) {
        carCategoryImplementation.delete(id);
        return response(OK, "Car is deleted");
    }

    private ResponseEntity<HttpResponse> response(HttpStatus httpStatus, String message) {
        return new ResponseEntity<>(new HttpResponse(httpStatus.value(), httpStatus, httpStatus.getReasonPhrase().toUpperCase(),
                message), httpStatus);
    }

}
