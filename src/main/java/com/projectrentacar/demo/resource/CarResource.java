package com.projectrentacar.demo.resource;


import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.projectrentacar.demo.domain.Car;
import com.projectrentacar.demo.repository.CarCategoryRepository;
import com.projectrentacar.demo.service.implementation.CarServiceImplementation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;



import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/car")
public class CarResource {

    private final CarServiceImplementation carService;
    private final CarCategoryRepository carCategoryRepository;
    private final ObjectMapper objectMapper;


    public CarResource(CarServiceImplementation carService, CarCategoryRepository carCategoryRepository, ObjectMapper objectMapper) {
        this.carService = carService;
        this.carCategoryRepository = carCategoryRepository;
        this.objectMapper = objectMapper;
    }

    @PostMapping(path = "/createCar")
    public ResponseEntity<Car> addNewCar( @RequestParam("licensePlate") String licensePlate,
                                          @RequestParam("name") String name,
                                          @RequestParam("description") String description,
                                          @RequestParam("fuel") String fuel,
                                          @RequestParam("currentPrice") String currentPrice,
                                          @RequestParam("availability") Boolean availability,
                                          @RequestParam("category") Long category)
    {

        ObjectMapper objectMapper = new ObjectMapper();



        var test = Double.parseDouble(String.valueOf(currentPrice));
        Car newCar = carService.addNewCar(licensePlate, name, description, fuel, test, availability, category);
        return new ResponseEntity<>(newCar, OK);
    }
}
