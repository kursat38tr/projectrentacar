package com.projectrentacar.demo.exception.domain;

import org.springframework.web.bind.annotation.ExceptionHandler;

public class EmailNotFoundException extends Exception {

    public EmailNotFoundException(String message) {
        super(message);
    }
}
