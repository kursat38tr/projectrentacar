package com.projectrentacar.demo.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "car_category")
@Getter
@Setter
public class CarCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("id")
    @Column(name = "id")
    private Long id;

    @JsonProperty("category_name")
    @Column(name = "category_name")
    private String categoryName;

    @JsonManagedReference
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "category", cascade = CascadeType.ALL)
    private Set<Car> cars;

    public CarCategory(){}

    public CarCategory(Long id, String categoryName) {
        this.id = id;
        this.categoryName = categoryName;
    }

    public CarCategory(Long id) {
        this.id = id;
    }

    public CarCategory(Long id, String categoryName, Set<Car> cars) {
        this.id = id;
        this.categoryName = categoryName;
        this.cars = cars;
    }

//    @Builder
//    @JsonCreator
//    public CarCategory (@JsonProperty("id") Long id,
//                @JsonProperty("category_name") String categoryName){
//
//        this.id = id;
//        this.categoryName = categoryName;
//
//    }

}
