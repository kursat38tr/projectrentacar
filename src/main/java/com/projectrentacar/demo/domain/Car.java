package com.projectrentacar.demo.domain;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import jdk.jfr.Category;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="car")
@Data
//@NoArgsConstructor
public class Car implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("id")
    @Column(name = "id")
    private Long id;


    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonProperty("category_id")
    @JoinColumn(name = "category_id", nullable = false)
    private CarCategory category;

    @JsonProperty("license_plate")
    @Column(name = "license_plate")
    private String licensePlate;

    @JsonProperty("name")
    @Column(name = "name")
    private String name;

    @JsonProperty("description")
    @Column(name = "description")
    private String description;

    @JsonProperty( "fuel")
    @Column(name = "fuel")
    private String fuel;

    @JsonProperty("image_url")
    @Column(name = "image_url")
    private String imageUrl;

    @JsonProperty("current_price")
    @Column(name = "current_price")
    private Double currentPrice;

    @JsonProperty("Availability")
    @Column(name = "Availability")
    private Boolean availability;


    @Builder
    @JsonCreator
    public Car (@JsonProperty("id") Long id,
                @JsonProperty("category_id")CarCategory category,
                @JsonProperty("license_plate") String licensePlate,
                @JsonProperty("name") String name,
                @JsonProperty("description") String description,
                @JsonProperty("fuel")String fuel,
                @JsonProperty("image_url") String imageUrl,
                @JsonProperty("current_price") Double currentPrice,
                @JsonProperty("Availability") Boolean availability){
        this.id= id;
        this.category = category;
        this.licensePlate = licensePlate;
        this.name = name;
        this.description = description;
        this.fuel = fuel;
        this.currentPrice = currentPrice;
        this.availability = availability;
    }



//    @Column(name = "date_created")
//    @CreationTimestamp
//    private Date dateCreated;



    public Car(Long id, CarCategory category, String licensePlate, String name, String description, String fuel, Double currentPrice, Boolean availability) {
        this.id = id;
        this.category = category;
        this.licensePlate = licensePlate;
        this.name = name;
        this.description = description;
        this.fuel = fuel;
        this.currentPrice = currentPrice;
        this.availability = availability;
    }

    public Car() {

    }


    public CarCategory getCategory() {
        return category;
    }

    public void setCategory(CarCategory category) {
        this.category = category;
    }

}
