package com.projectrentacar.demo.repository;

import com.projectrentacar.demo.domain.Car;

import com.projectrentacar.demo.domain.CarCategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestParam;


@CrossOrigin("http://localhost:4200")
public interface CarRepository extends JpaRepository<Car, Long> {

    Page<Car> findByCategoryId(@RequestParam("id") Long id, Pageable pageable);

    Page<Car> findByNameContaining(@RequestParam("name") String name, Pageable pageable);

    Long findCategoryIdByName(String name);

    Car findCarById(Long id);

}
